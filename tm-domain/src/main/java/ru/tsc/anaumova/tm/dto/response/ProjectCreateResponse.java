package ru.tsc.anaumova.tm.dto.response;

import org.jetbrains.annotations.Nullable;
import ru.tsc.anaumova.tm.model.Project;

public final class ProjectCreateResponse extends AbstractProjectResponse {

    public ProjectCreateResponse(@Nullable final Project project) {
        super(project);
    }

}