package ru.tsc.anaumova.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.tsc.anaumova.tm.dto.request.UserLoginRequest;
import ru.tsc.anaumova.tm.dto.request.UserLogoutRequest;
import ru.tsc.anaumova.tm.dto.request.UserProfileRequest;
import ru.tsc.anaumova.tm.dto.response.UserLoginResponse;
import ru.tsc.anaumova.tm.dto.response.UserLogoutResponse;
import ru.tsc.anaumova.tm.dto.response.UserProfileResponse;

public interface IAuthEndpoint {

    @NotNull
    UserLoginResponse login(@NotNull UserLoginRequest request);

    @NotNull
    UserLogoutResponse logout(@NotNull UserLogoutRequest request);

    @NotNull
    UserProfileResponse profile(@NotNull UserProfileRequest request);

}