package ru.tsc.anaumova.tm.dto.response;

import org.jetbrains.annotations.Nullable;
import ru.tsc.anaumova.tm.model.Task;

public final class TaskRemoveByIndexResponse extends AbstractTaskResponse {

    public TaskRemoveByIndexResponse(@Nullable final Task task) {
        super(task);
    }

}