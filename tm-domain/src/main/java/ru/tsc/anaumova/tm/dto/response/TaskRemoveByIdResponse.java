package ru.tsc.anaumova.tm.dto.response;

import org.jetbrains.annotations.Nullable;
import ru.tsc.anaumova.tm.model.Task;

public final class TaskRemoveByIdResponse extends AbstractTaskResponse {

    public TaskRemoveByIdResponse(@Nullable final Task task) {
        super(task);
    }

}