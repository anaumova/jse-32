package ru.tsc.anaumova.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.tsc.anaumova.tm.dto.request.AbstractRequest;
import ru.tsc.anaumova.tm.dto.response.AbstractResponse;

@FunctionalInterface
public interface Operation<RQ extends AbstractRequest, RS extends AbstractResponse> {

    RS execute(@NotNull RQ request);

}