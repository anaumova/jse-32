package ru.tsc.anaumova.tm.client;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.anaumova.tm.api.endpoint.IProjectTaskEndpoint;
import ru.tsc.anaumova.tm.dto.request.TaskBindToProjectRequest;
import ru.tsc.anaumova.tm.dto.request.TaskUnbindFromProjectRequest;
import ru.tsc.anaumova.tm.dto.response.TaskBindToProjectResponse;
import ru.tsc.anaumova.tm.dto.response.TaskUnbindFromProjectResponse;

public class ProjectTaskEndpointClient extends AbstractEndpointClient implements IProjectTaskEndpoint {

    @NotNull
    @Override
    @SneakyThrows
    public TaskBindToProjectResponse bindTaskToProject(@NotNull final TaskBindToProjectRequest request) {
        return call(request, TaskBindToProjectResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskUnbindFromProjectResponse unbindTaskFromProject(@NotNull final TaskUnbindFromProjectRequest request) {
        return call(request, TaskUnbindFromProjectResponse.class);
    }

}